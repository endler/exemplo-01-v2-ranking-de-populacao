# Flutter Exemplo 1 - Ranking de População

Aplicação Flutter que:

- Lista alguns países.
- Suas respectivas bandeiras.
- Tamanho da população.
- Posição do país no ranking de população.

## Pré-requisitos

- [Flutter SDK](https://flutter.dev/docs/get-started/install)
- IDE para desenvolvimento. Sugestão: [Visual Studio Code](https://code.visualstudio.com/)

OBS: Você precisará instalar o Android Studio, pois ele gerencia a instalação do Android SDK e do emulador para execução das aplicações.

## Contatos

- Professor Markus Endler (endler@inf.puc-rio.br)

Disciplina INF1300 - Desenvolvimento Mobile com Flutter.
